package net.freebytes.websocket.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.freebytes.websocket.common.dto.WsMessage;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class WebSocketService {

    private final SimpMessagingTemplate wsTemplate;

    public WebSocketService(SimpMessagingTemplate wsTemplate) {
        this.wsTemplate = wsTemplate;
    }


    /**
     * 群发消息
     *
     * @param prefix 客户端订阅地址的前缀信息  如：/insert-user
     * @param item   客户端订阅地址的补充信息  如：get   与前缀拼接起来就是  /insert-user/get
     * @param obj    消息内容
     * @throws Exception
     * @author 千里明月
     * @date 2021/8/8
     **/
    public void sendToAll(String prefix, String item, Object obj) throws Exception {
        ObjectMapper om = new ObjectMapper();
        wsTemplate.convertAndSend(prefix + item, om.writeValueAsString(obj));
    }

    /**
     * 指定用户推送
     *
     * @param prefix 客户端订阅地址的前缀信息  如：/insert-user
     * @param item   客户端订阅地址的补充信息  如：get   与前缀拼接起来就是  /insert-user/get
     * @param msg    消息内容
     * @author 千里明月
     * @date 2021/8/8
     **/
    public void sentToUsers(String prefix, String item, WsMessage msg) throws Exception {
        ObjectMapper om = new ObjectMapper();
        List<String> users = msg.getUsers();
        if (users == null) {
            return;
        }
        for (String user : users) {
            //这里会拼接上用户的id作为ws地址的补充信息，需要前端在订阅的时候，写上这个id
            wsTemplate.convertAndSend(prefix + item + "/" + user, om.writeValueAsString(msg.getMessage()));
        }
    }
}
