package net.freebytes.websocket.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 千里明月
 * @date 2021/8/8 17:52
 */
@EnableFeignClients({"net.freebytes.websocket.api"})
@EnableAutoConfiguration
@ComponentScan({"net.freebytes.websocket"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
